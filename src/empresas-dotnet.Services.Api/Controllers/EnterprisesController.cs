﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Domain.Enterprise.Repository;
using empresas_dotnet.Domain.Enterprise.Services;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotnet.Services.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService _enterpriseService;

        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            _enterpriseService = enterpriseService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(
            [FromQuery(Name ="enterprise_types")]int? type,
            [FromQuery(Name = "name")] string name)
        {
            var enterprises = await _enterpriseService.GetByTypeAndNameAsync(type, name);

            return Ok(enterprises);
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<IActionResult> Details(int id)
        {
            var enterprises = await _enterpriseService.GetByIdAsync(id);

            return Ok(enterprises);
        }
    }
}
