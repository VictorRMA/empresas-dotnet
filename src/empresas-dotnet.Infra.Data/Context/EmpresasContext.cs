﻿using System;
using System.Collections.Generic;
using System.Text;
using empresas_dotnet.Domain.Enterprise;
using empresas_dotnet.Infra.Data.Extensions;
using empresas_dotnet.Infra.Data.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace empresas_dotnet.Infra.Data.Context
{
    public class EmpresasContext : DbContext
    {
        private readonly IHostingEnvironment _env;

        public EmpresasContext(IHostingEnvironment env)
        {
            _env = env;
        }

        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<EnterpriseType> Types { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EnterpriseMap());
            modelBuilder.ApplyConfiguration(new EnterpriseTypeMap());

            modelBuilder.Seed();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // get the configuration from the app settings
            var config = new ConfigurationBuilder()
                .SetBasePath(_env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .Build();

            // define the database to use
            optionsBuilder.UseMySql(config.GetConnectionString("DefaultConnection"));
        }
    }
}
