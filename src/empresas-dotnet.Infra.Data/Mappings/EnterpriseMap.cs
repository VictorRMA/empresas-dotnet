﻿using System;
using System.Collections.Generic;
using System.Text;
using empresas_dotnet.Domain.Enterprise;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Infra.Data.Mappings
{
    public class EnterpriseMap : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.ToTable("Enterprises");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .IsRequired();

            builder.Property(p => p.Description)
                .IsRequired();

            builder.Property(p => p.Email);

            builder.Property(p => p.Facebook);

            builder.Property(p => p.Twitter);

            builder.Property(p => p.LinkedIn);

            builder.Property(p => p.Phone);

            builder.Property(p => p.Photo);

            builder.Property(p => p.Value)
                .IsRequired();

            builder.Property(p => p.Shares)
                .IsRequired();

            builder.Property(p => p.SharePrice)
                .IsRequired();

            builder.Property(p => p.City)
                .IsRequired();

            builder.Property(p => p.Country)
                .IsRequired();

            builder.HasOne(p => p.EnterpriseType)
                .WithMany()
                .HasForeignKey(p => p.TypeId);
        }
    }
}
