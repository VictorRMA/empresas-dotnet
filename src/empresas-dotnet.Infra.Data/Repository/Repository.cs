﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using empresas_dotnet.Domain.Core.Models;
using empresas_dotnet.Domain.Interfaces;
using empresas_dotnet.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Infra.Data.Repository
{
    public class Repository<T> : IAsyncRepository<T> where T : Entity
    {
        protected readonly EmpresasContext _dbContext;

        public Repository(EmpresasContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task<IReadOnlyList<T>> ListAllAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<IReadOnlyList<T>> ListAsync(Expression<Func<T, bool>> criteria)
        {
            return await _dbContext.Set<T>().Where(criteria).ToListAsync();
        }

        public Task<T> AddAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
