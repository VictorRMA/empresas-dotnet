﻿using System;
using System.Collections.Generic;
using System.Text;
using empresas_dotnet.Domain.Enterprise;
using empresas_dotnet.Domain.Enterprise.Repository;
using empresas_dotnet.Infra.Data.Context;

namespace empresas_dotnet.Infra.Data.Repository
{
    public class EnterpriseRepository : Repository<Enterprise>, IEnterpriseRepository
    {
        public EnterpriseRepository(EmpresasContext dbContext) : base(dbContext)
        {
        }
    }
}
