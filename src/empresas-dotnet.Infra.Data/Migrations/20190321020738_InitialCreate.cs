﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace empresas_dotnet.Infra.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Types",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Types", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enterprises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Facebook = table.Column<string>(nullable: true),
                    Twitter = table.Column<string>(nullable: true),
                    LinkedIn = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Value = table.Column<int>(nullable: false),
                    Shares = table.Column<int>(nullable: false),
                    SharePrice = table.Column<decimal>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enterprises_Types_TypeId",
                        column: x => x.TypeId,
                        principalTable: "Types",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Types",
                columns: new[] { "Id", "Name" },
                values: new object[] { 21, "Software" });

            migrationBuilder.InsertData(
                table: "Enterprises",
                columns: new[] { "Id", "City", "Country", "Description", "Email", "Facebook", "LinkedIn", "Name", "Phone", "Photo", "SharePrice", "Shares", "Twitter", "TypeId", "Value" },
                values: new object[,]
                {
                    { 1, "Santiago", "Chile", @"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We 
                                      want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the 
                                      creation of citizen green areas. With this we are creating smarter cities from the people and at the same 
                                      time the forest city.  Urbanatika, Agro-Urban Industry", null, null, null, "AllRide", null, "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg", 10000m, 100, null, 21, 0 },
                    { 2, "Santiago", "Chile", @"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We 
                                      want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the 
                                      creation of citizen green areas. With this we are creating smarter cities from the people and at the same 
                                      time the forest city.  Urbanatika, Agro-Urban Industry", null, null, null, "AllRide2", null, "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg", 10000m, 100, null, 21, 0 },
                    { 3, "Santiago", "Chile", @"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We 
                                      want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the 
                                      creation of citizen green areas. With this we are creating smarter cities from the people and at the same 
                                      time the forest city.  Urbanatika, Agro-Urban Industry", null, null, null, "AllRide3", null, "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg", 10000m, 100, null, 21, 0 },
                    { 4, "Santiago", "Chile", @"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We 
                                      want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the 
                                      creation of citizen green areas. With this we are creating smarter cities from the people and at the same 
                                      time the forest city.  Urbanatika, Agro-Urban Industry", null, null, null, "AllRide4", null, "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg", 10000m, 100, null, 21, 0 },
                    { 5, "Santiago", "Chile", @"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We 
                                      want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the 
                                      creation of citizen green areas. With this we are creating smarter cities from the people and at the same 
                                      time the forest city.  Urbanatika, Agro-Urban Industry", null, null, null, "AllRide5", null, "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg", 10000m, 100, null, 21, 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enterprises_TypeId",
                table: "Enterprises",
                column: "TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Enterprises");

            migrationBuilder.DropTable(
                name: "Types");
        }
    }
}
