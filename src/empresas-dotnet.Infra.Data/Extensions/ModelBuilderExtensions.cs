﻿using System;
using System.Collections.Generic;
using System.Text;
using empresas_dotnet.Domain.Enterprise;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Infra.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EnterpriseType>().HasData(
                new EnterpriseType(21, "Software")
            );

            modelBuilder.Entity<Enterprise>().HasData(
                Enterprise.CreateDump(1, "AllRide"),
                Enterprise.CreateDump(2, "AllRide2"),
                Enterprise.CreateDump(3, "AllRide3"),
                Enterprise.CreateDump(4, "AllRide4"),
                Enterprise.CreateDump(5, "AllRide5")
            );
        }
    }
}
