﻿using empresas_dotnet.Domain.Enterprise.Repository;
using empresas_dotnet.Domain.Enterprise.Services;
using empresas_dotnet.Infra.Data.Context;
using empresas_dotnet.Infra.Data.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace empresas_dotnet.Infra.Data.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddDbContext<EmpresasContext>();

            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();

            services.AddScoped<IEnterpriseService, EnterpriseService>();
        }
    }
}
