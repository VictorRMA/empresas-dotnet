﻿using System;
using System.Collections.Generic;
using System.Text;
using empresas_dotnet.Domain.Core.Models;

namespace empresas_dotnet.Domain.Enterprise
{
    public class EnterpriseType : Entity
    {
        public string Name { get; private set; }


        //Empty ctor for EF
        protected EnterpriseType() { }

        public EnterpriseType(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
