﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using empresas_dotnet.Domain.Enterprise.Repository;

namespace empresas_dotnet.Domain.Enterprise.Services
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        public async Task<Enterprise> GetByIdAsync(int id)
        {
            return await _enterpriseRepository.GetByIdAsync(id);
        }

        public async Task<IReadOnlyList<Enterprise>> GetByTypeAndNameAsync(int? type, string name)
        {
            return await _enterpriseRepository.ListAsync(e => (!type.HasValue || e.TypeId == type) && 
                                                              (name == null || e.Name.Contains(name, StringComparison.CurrentCultureIgnoreCase)));
        }
    }
}
