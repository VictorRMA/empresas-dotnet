﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace empresas_dotnet.Domain.Enterprise.Services
{
    public interface IEnterpriseService
    {
        Task<Enterprise> GetByIdAsync(int id);

        Task<IReadOnlyList<Enterprise>> GetByTypeAndNameAsync(int? type, string name);
    }
}
