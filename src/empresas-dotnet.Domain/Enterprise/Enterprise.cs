﻿using empresas_dotnet.Domain.Core.Models;

namespace empresas_dotnet.Domain.Enterprise
{
    public class Enterprise : Entity
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public string Email { get; private set; }

        public string Facebook { get; private set; }

        public string Twitter { get; private set; }

        public string LinkedIn { get; private set; }

        public string Phone { get; private set; }

        public string Photo { get; private set; }

        public int Value { get; private set; }

        public int Shares { get; private set; }

        public decimal SharePrice { get; private set; }

        public string City { get; private set; }

        public string Country { get; private set; }

        public int TypeId { get; private set; }

        public virtual EnterpriseType EnterpriseType { get; private set; }

        //Empty ctor for EF
        protected Enterprise() { }

        public static Enterprise CreateDump(int id, string name)
        {
            return new Enterprise
            {
                Id = id,
                Name = name,
                Description =
                    @"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We 
                      want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the 
                      creation of citizen green areas. With this we are creating smarter cities from the people and at the same 
                      time the forest city.  Urbanatika, Agro-Urban Industry",
                Photo = "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg",
                Value = 0,
                Shares = 100,
                SharePrice = 10000,
                City = "Santiago",
                Country = "Chile",
                TypeId = 21
            };
        }
    }
}
