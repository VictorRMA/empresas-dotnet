﻿using System;
using System.Collections.Generic;
using System.Text;
using empresas_dotnet.Domain.Interfaces;

namespace empresas_dotnet.Domain.Enterprise.Repository
{
    public interface IEnterpriseRepository : IAsyncRepository<Enterprise>
    {
    }
}
